import SwiftUI

extension Color {
    // Brand colors
    static let primaryColor = Color("Primary")
    static let backgroundColor = Color("Background")
    
    // Swatch pallette
    static let colorGrey = Color("Grey")
}
