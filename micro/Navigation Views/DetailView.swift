//
//  DetailView.swift
//  micro
//
//  Created by Eduardo Alvarez on 2020-05-12.
//  Copyright © 2020 Eduardo Alvarez. All rights reserved.
//

import SwiftUI

struct DetailView: View {
    var story:Story
    
    var body: some View {
        VStack {
            story.image
                .resizable()
                .aspectRatio(contentMode: .fit)
            
            Text(story.caption)
                .font(.footnote)
                .foregroundColor(.colorGrey)
                .padding(.top)
                .padding(.bottom, 8)
                .padding(.horizontal)
            
            ReadView(url: story.url).padding(.horizontal)
            
            Spacer()
        }.navigationBarTitle(Text(story.category), displayMode: .inline)
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(story: newscastData[2])
    }
}
