import SwiftUI

struct ContentView: View {
    @EnvironmentObject var appData: AppData
    
    var story:Story { return appData.story }
    
    var body: some View {
        NavigationView {
            VStack {
                Spacer()
                
                HeadingsView(story: story).padding(.horizontal)
                
                if (story.media) {
                    NavigationLink(destination: DetailView(story:story)) {
                        ImageView(image: story.image)
                    }
                    .offset(y: 100)
                    .padding(.top, -100)
                    .zIndex(1)
                }
                else {
                    ReadView(url: story.url)
                        .padding(.horizontal)
                        .padding(.bottom)
                }
                
                VStack {
                    Spacer()
                    ControlView()
                    Image("logo-small").padding()
                }
                .frame(maxWidth: .infinity, maxHeight: 370)
                .background(Color.backgroundColor)
            }
            .edgesIgnoringSafeArea(.vertical)
        }        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(AppData())
    }
}
