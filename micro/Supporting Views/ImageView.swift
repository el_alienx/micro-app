import SwiftUI

struct ImageView: View {
    var image:Image
    
    var body: some View {
        image
            .renderingMode(.original)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 200, height: 200)
            .cornerRadius(12)
            .shadow(radius: 8)
    }
}

struct ImageView_Previews: PreviewProvider {
    static var previews: some View {
        ImageView(image: Image("default-asset"))
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
