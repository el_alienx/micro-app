//
//  BackgroundView.swift
//  micro
//
//  Created by Eduardo Alvarez on 2020-05-08.
//  Copyright © 2020 Eduardo Alvarez. All rights reserved.
//

import SwiftUI

struct BackgroundView: View {
    var screenHeight:CGFloat
    
    var body: some View {
        VStack {
            Spacer()
            
            Rectangle().fill(Color(red: 0.11, green: 0.11, blue: 0.118))
                .frame(height: self.screenHeight/2)
            
            Image("logo-cobranding")
                .offset(y: -60)
                .padding(.bottom, -60)
        }.edgesIgnoringSafeArea(.bottom)
    }
}

struct BackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundView(screenHeight: 800)
            .previewLayout(.fixed(width: 400, height: 400))
    }
}
