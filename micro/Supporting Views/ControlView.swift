import SwiftUI
import AVFoundation


struct ControlView: View {
    // State
    @EnvironmentObject var appData:AppData
    @State var showPlayButton = true
    @State var isNewsCastComplete = false
    
    // Computed
    var textToSpeak:String {
        return appData.story.linker + appData.story.category
    }
    
    var narrator = Narrator()
    
    var body: some View {
        HStack {
            Button(action: onMainButton) {
                Image(showPlayButton ? "play" : "pause").renderingMode(.original)
            }
        }
    }
    
    func onMainButton() {
        isNewsCastComplete = false
        showPlayButton ? play() : pause()
        showPlayButton.toggle()
    }
    
    func play() {
        narrator.requestPlay(string: textToSpeak)
        narrator.onFinish = changeStory
    }
    
    func pause() {
        narrator.pause()
    }
    
    func changeStory() {
        if (appData.index + 1 < appData.count - 1 && !isNewsCastComplete) {
            appData.index+=1
            play()
        }
        else {
            isNewsCastComplete = true
            showPlayButton = true
            appData.index = 0
        }
    }
}

struct ControlView_Previews: PreviewProvider {
    static var previews: some View {
        ControlView()
            .background(Color.primaryColor)
            .previewLayout(.sizeThatFits)
    }
}
