import SwiftUI

struct ProgressBarView: View {
    var body: some View {
        HStack{
            Text("0:00")
                .font(.footnote)
                .foregroundColor(.colorGrey)
                .padding()
            
            Image("sound-wave")
            
            Text("1:30")
                .font(.footnote)
                .foregroundColor(.colorGrey)
                .padding()
        }
    }
}

struct PlayerView_Previews: PreviewProvider {
    static var previews: some View {
        ProgressBarView()
            .background(Color.primaryColor)
            .previewLayout(.sizeThatFits)
    }
}
