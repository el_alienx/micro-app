import SwiftUI

struct ArticleView: View {
    let story: Story
    
    var body: some View {
        VStack {
            HeadingsView(story: self.story)
            
            if (self.story.media) {
                NavigationLink(destination: DetailView(image: self.story.image )) {
                    ImageView(image: self.story.image)
                }.buttonStyle(PlainButtonStyle())
            }
            else {
                HStack{
                    Button(action: {UIApplication.shared.open(URL(string: self.story.url)!)}) {
                        Image("link")
                    }
                    .buttonStyle(PlainButtonStyle())
                    
                    Spacer()
                }
            }
        }
    }
}

struct ArticleView_Previews: PreviewProvider {
    static var previews: some View {
        Group{
            ArticleView(story: newscastData[0])
            ArticleView(story: newscastData[1])
        }
        .previewLayout(.sizeThatFits)
    }
}
