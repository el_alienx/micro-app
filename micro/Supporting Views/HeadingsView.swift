import SwiftUI

struct HeadingsView: View {
    let story:Story
    
    var body: some View {
        VStack(alignment:  story.media ? .center : .leading){
            Text(story.category.uppercased())
                .font(.custom("Graphik-Regular", size: 15))
                .foregroundColor(.colorGrey)
                .padding(.bottom, 8)
            
            Text(story.title)
                .font(.custom("Graphik-Semibold", size: 36))
                .foregroundColor(.primaryColor)
                .multilineTextAlignment(story.media ? .center : .leading)
                .lineLimit(3)
        }
        .frame(maxWidth: .infinity, alignment: story.media ? .center : .leading)
    }
}

struct HeadingsView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            HeadingsView(story: newscastData[0])
            HeadingsView(story: newscastData[1])
        }
        .previewLayout(.sizeThatFits)
        .padding()
    }
}
