import SwiftUI

struct ReadView: View {
    let url: URL
    
    var body: some View {
        HStack{
            Button( action: { UIApplication.shared.open(self.url)}) {
                Image("link").renderingMode(.original)
            }
            
            Spacer()
        }
    }
}

struct ReadView_Previews: PreviewProvider {
    static var previews: some View {
        ReadView(url: newscastData[0].url)
            .padding()
            .previewLayout(.sizeThatFits)
    }
}
