import SwiftUI
import Combine

final class AppData: ObservableObject  {
    // Constanst
    let newscast = newscastData

    // State
    @Published var index = 0
    @Published var goodbye = "Thank you for listening this micro, good bye!"
    
    // Computed properties
    var count:Int {
        return newscast.count
    }
    var story:Story {
        return newscast[index]
    }
    
    // Methods
    public func increment() {
        if (index < count - 1) { index+=1 }
    }
    
    public func decrement() {
        if (index > 0) { index-=1 }
    }
}
