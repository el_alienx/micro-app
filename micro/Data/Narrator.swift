import SwiftUI
import AVFoundation

class Narrator: NSObject {
    var synthesizer = AVSpeechSynthesizer()
    var utterance = AVSpeechUtterance(string: "")
        
    public var onFinish:(()->())?
    public var onCancel:(()->())?
    
    override init() {
        super.init()
        synthesizer.delegate = self
        utterance.postUtteranceDelay = 0.5
    }
    
    // Public Methods
    public func requestPlay(string: String ) {
        synthesizer.isPaused ? resume() : play(string)
    }
    
    public func pause()->Void {
        synthesizer.pauseSpeaking(at: .immediate)
    }

    public func stop() {
        synthesizer.stopSpeaking(at: .immediate)
    }
    
    // Private Methods
    private func play(_ string: String) {
        utterance = AVSpeechUtterance(string: string)
        synthesizer.speak(utterance)
    }
    
    private func resume() {
        synthesizer.continueSpeaking()
    }
}

extension Narrator: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        onFinish?()
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance) {
        onCancel?()
    }
}
