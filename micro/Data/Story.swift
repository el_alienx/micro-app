import SwiftUI

struct Story: Hashable, Codable, Identifiable {
    var id: Int
    var category: String
    fileprivate var imageName: String?
    var caption: String = "Sorry, this image does not have any caption"
    var linker: String
    var media: Bool
    var text: String
    var title: String
    var url: URL
}

extension Story {
    var image: Image {
        ImageStore.shared.image(name: imageName ?? "default-resource")
    }
}
